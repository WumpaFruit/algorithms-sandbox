package graphtheory;

import java.util.ArrayList;
import java.util.List;

public class AdjListRow {
	private Vertex source;
	private List<Edge> edges;
	
	public AdjListRow(Vertex source) {
		this.source = source;
		edges = new ArrayList<>();
	}
	
	public void addEdge(Vertex destination) {
		addEdge(destination, 1);
	}
	
	public void addEdge(Vertex destination, double weight) {
		for (Edge e: edges) {
			//Already added
			if (e.getDestination().equals(destination)) {
				return;
			}
		}
		
		edges.add(new Edge(source, destination, weight));
	}
	
	public void removeEdge(Vertex destination) {
		for (int i = 0; i < edges.size(); i++) {
			if (edges.get(i).getDestination().equals(destination)) {
				edges.remove(i);
				i--;
			}
		}
	}
	
	public Vertex getSource() {
		return source;
	}
	
	public List<Edge> getEdges() {
		return edges;
	}
}
