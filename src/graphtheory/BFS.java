package graphtheory;

import java.util.LinkedList;
import java.util.Queue;

public class BFS {
	public static Graph doBFSAdjList(Graph g, Vertex start) {
		Graph tree = new GraphAdjList(g.isDirected());
		
		Queue<Vertex> q = new LinkedList<>();
		q.offer(start);
		tree.addVertex(start);
		
		
		while (!q.isEmpty()) {
			Vertex u = q.poll();
			
			for (Edge e: g.getEdges(u)) {
				Vertex v = e.getDestination();
				
				if (!v.isVisited()) {
					tree.addVertex(v);
					tree.addEdge(u, v);
					q.offer(v);
					v.setVisited(true);
				}
			}
			
			u.setVisited(true);
		}
		
		return tree;
	}
}
