package graphtheory;

public class Edge {
	private Vertex source, destination;
	private double weight;
	
	public Edge(Vertex u, Vertex v) {
		this(u, v, 1);
	}
	
	public Edge(Vertex u, Vertex v, double weight) {
		source = u;
		destination = v;
		this.weight = weight;
	}
	
	public Vertex getSource() {
		return source;
	}
	
	public Vertex getDestination() {
		return destination;
	}
	
	public double getWeight() {
		return weight;
	}
}
