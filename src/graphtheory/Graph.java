package graphtheory;

import java.util.List;

public abstract class Graph {
	protected boolean directed;
	
	public Graph(boolean directed) {
		this.directed = directed;
	}
	
	public abstract void addVertex(Vertex v);
	
	public abstract void addEdge(Vertex u, Vertex v);
	public abstract void addEdge(Vertex u, Vertex v, double weight);
	
	public abstract void removeEdge(Vertex u, Vertex v);
	
	public abstract List<Edge> getEdges(Vertex v);
	public abstract List<Vertex> getVertices();
	
	public abstract String toString();
	
	public boolean isDirected() {
		return directed;
	}
}
