package graphtheory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GraphAdjList extends Graph {
	List<AdjListRow> adjacencyList;
	
	public GraphAdjList(boolean directed) {
		super(directed);
		adjacencyList = new LinkedList<>();
	}

	@Override
	public void addVertex(Vertex v) {
		for (AdjListRow row: adjacencyList) {
			if (row.getSource() == v) {	//This vertex is already in the graph
				return;
			}
		}
		
		//Otherwise, add the vertex to the graph
		AdjListRow newRow = new AdjListRow(v);
		adjacencyList.add(newRow);
		
	}

	public void addEdge(Vertex u, Vertex v) {
		addEdge(u, v, 1);
	}
	
	@Override
	public void addEdge(Vertex u, Vertex v, double weight) {
		for (AdjListRow row: adjacencyList) {
			if (row.getSource().equals(u)) {
				row.addEdge(v, weight);
			}
			
			if (!directed && row.getSource().equals(v)) {
				row.addEdge(u, weight);
			}
		}
	}
	
	@Override
	public void removeEdge(Vertex u, Vertex v) {
		for (AdjListRow row: adjacencyList) {
			if (row.getSource().equals(u)) {
				row.removeEdge(v);
			}
			
			if (!directed && row.getSource().equals(v)) {
				row.removeEdge(u);
			}
		}
		
	}
	
	public List<Edge> getEdges(Vertex v) {
		for (AdjListRow row: adjacencyList) {
			if (row.getSource().equals(v)) {
				return row.getEdges();
			}
		}
		
		return new LinkedList<>();
	}
	
	@Override
	public List<Vertex> getVertices() {
		List<Vertex> vertices = new ArrayList<>();
		
		for (AdjListRow row: adjacencyList) {
			vertices.add(row.getSource());
		}
		
		return vertices;
	}

	@Override
	public String toString() {
		String s = "";
		for (AdjListRow row: adjacencyList) {
			s += row.getSource() + ": ";
			if (row.getEdges().size() > 0) {
				for (int i = 0; i < row.getEdges().size(); i++) {
					s += row.getEdges().get(i).getDestination() + "(" + row.getEdges().get(i).getWeight() + ")";
				}
			}
			s += "\n";
		}
		
		return s;
	}
	
}
