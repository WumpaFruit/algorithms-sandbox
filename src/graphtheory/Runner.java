package graphtheory;

import shortestpaths.Dijkstra;

public class Runner {

	public static void main(String[] args) {
		Graph g = new GraphAdjList(false);
		
		Vertex r = new Vertex("r");
		Vertex s = new Vertex("s");
		Vertex t = new Vertex("t");
		Vertex u = new Vertex("u");
		Vertex v = new Vertex("v");
		Vertex w = new Vertex("w");
		Vertex x = new Vertex("x");
		Vertex y = new Vertex("y");
		
		g.addVertex(r);
		g.addVertex(s);
		g.addVertex(t);
		g.addVertex(u);
		g.addVertex(v);
		g.addVertex(w);
		g.addVertex(x);
		g.addVertex(y);
		
		g.addEdge(r, s);
		g.addEdge(r, v);
		g.addEdge(s, r);
		g.addEdge(s, w);
		g.addEdge(t, u);
		g.addEdge(t, w);
		g.addEdge(t, x);
		g.addEdge(u, t);
		g.addEdge(u, y);
		g.addEdge(v, r);
		g.addEdge(w, s);
		g.addEdge(w, t);
		g.addEdge(w, x);
		g.addEdge(x, t);
		g.addEdge(x, w);
		g.addEdge(x, y);
		g.addEdge(y, u);
		g.addEdge(y, x);
		
		Graph g2 = new GraphAdjList(false);
		Vertex a = new Vertex("a");
		Vertex b = new Vertex("b");
		Vertex c = new Vertex("c");
		Vertex d = new Vertex("d");
		Vertex e = new Vertex("e");
		Vertex f = new Vertex("f");
		
		g2.addVertex(a);
		g2.addVertex(b);
		g2.addVertex(c);
		g2.addVertex(d);
		g2.addVertex(e);
		g2.addVertex(f);
		g2.addVertex(s);
		g2.addVertex(t);
		
		g2.addEdge(s, d, 4);
		g2.addEdge(s, a, 4);
		g2.addEdge(d, e, 1);
		g2.addEdge(d, c, 2);
		g2.addEdge(e, f, 2);
		g2.addEdge(a, b, 1);
		g2.addEdge(b, c, 1);
		g2.addEdge(c, f, 1);
		g2.addEdge(f, t, 4);
		
		Graph g3 = new GraphAdjList(false);
		g3.addVertex(s);
		g3.addVertex(t);
		g3.addVertex(a);
		g3.addVertex(b);
		g3.addVertex(c);
		g3.addVertex(d);
		g3.addVertex(e);
		
		//g3.addEdge(s, c, 2);
		g3.addEdge(s, a, 1);
		g3.addEdge(a, b, 1);
		g3.addEdge(b, t, 1);
		g3.addEdge(c, t, 1);
		g3.addEdge(s, d, 2);
		g3.addEdge(d, t, 1);
		g3.addEdge(s, c, 2);
		g3.addEdge(s, e, 2);
		g3.addEdge(e, t, 2);
		
		System.out.println(g3);
		
		System.out.println(Dijkstra.findShortestPathWithFewestEdges(g3, s, t));
		//Dijkstra.doDijkstra(g3, s);
		
		//for (Vertex v1: g3.getVertices()) {
			//System.out.println("usp["+v1+"]="+v1.getUsp());
		//}

	}

}
