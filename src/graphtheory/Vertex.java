package graphtheory;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
	private String name;
	private boolean visited;
	private boolean usp;
	private double d;
	private List<Vertex> pi;
	
	public Vertex(String name) {
		this.name = name;
		visited = false;
		d = 0;
		usp = false;
		pi = new ArrayList<>();
	}
	
	public void reset() {
		visited = false;
		usp = true;
		d = Double.MAX_VALUE;
		pi = new ArrayList<>();
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
	
	public void setVisited(boolean visited) {
		this.visited = visited;
	}
	
	public boolean isVisited() {
		return visited;
	}

	public double getD() {
		return d;
	}

	public void setD(double d) {
		this.d = d;
	}

	public List<Vertex> getPi() {
		return pi;
	}

	public void addToPi(Vertex pi) {
		this.pi.add(pi);
	}
	
	public boolean getUsp() {
		return usp;
	}
	
	public void setUsp(boolean usp) {
		this.usp = usp;
	}
}
