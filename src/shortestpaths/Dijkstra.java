package shortestpaths;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Comparator;

import graphtheory.*;

public class Dijkstra {
	public static List<Vertex> findShortestPathWithFewestEdges(Graph g, Vertex s, Vertex t) {
		Graph h = doDijkstraAll(g, s);
		doDijkstra(h, s);
		
		Vertex v = t;
		LinkedList<Vertex> fewestEdgesShortestPath = new LinkedList<>();
		
		while (!v.getPi().isEmpty()) {
			fewestEdgesShortestPath.addFirst(v);
			v = v.getPi().get(0);
		}
		
		fewestEdgesShortestPath.addFirst(v);
		
		return fewestEdgesShortestPath;
	}
	
	private static void printPath(Vertex t) {
		if (t.getPi() != null && t.getPi().size() > 0) {
			printPath(t.getPi().get(0));
			System.out.print("-->" + t);
		} else {
			System.out.print(t);
		}
	}
	
	public static void doDijkstra(Graph g, Vertex s) {
		List<Vertex> V = g.getVertices();
		
		for (Vertex v: V) {
			v.reset();
			
			if (v.equals(s)) {
				v.setD(0);
				v.setUsp(true);
			}
		}
		
		Set<Vertex> S = new HashSet<>();
		
		PriorityQueue<Vertex> pq = new PriorityQueue<>(V.size(), new VertexComparator());
		
		for (Vertex v: V) {
			pq.add(v);
		}
		
		while(!pq.isEmpty()) {
			Vertex u = pq.poll();
			
			S.add(u);
			
			for (Edge e: g.getEdges(u)) {
				Vertex v = e.getDestination();
				
				if (!S.contains(v)) {
					relax(pq, e);
				}
			}
		}
	}
	
	public static Graph doDijkstraAll(Graph g, Vertex s) {
		List<Vertex> V = g.getVertices();
		
		Graph h = new GraphAdjList(g.isDirected());
		
		for (Vertex v: V) {
			v.reset();
			
			if (v.equals(s)) {
				v.setD(0);
			}
		}
		
		Set<Vertex> S = new HashSet<>();
		
		PriorityQueue<Vertex> pq = new PriorityQueue<>(V.size(), new VertexComparator());
		
		for (Vertex v: V) {
			pq.add(v);
		}
		
		while(!pq.isEmpty()) {
			Vertex u = pq.poll();
			h.addVertex(u);
			
			S.add(u);
			
			for (Edge e: g.getEdges(u)) {
				Vertex v = e.getDestination();
				
				if (!S.contains(v)) {
					relaxAll(pq, e, h);
				}
			}
		}
		
		return h;
	}
	
	public static void relax(PriorityQueue<Vertex> pq, Edge e) {
		Vertex u = e.getSource();
		Vertex v = e.getDestination();
		
		if (u.getD() + e.getWeight() < v.getD()) {
			pq.remove(v);
			v.setD(u.getD() + e.getWeight());
			v.addToPi(u);
			pq.add(v);
		} else if (u.getD() + e.getWeight() == v.getD()) {
			v.setUsp(false);
		}
	}
	
	public static void relaxAll(PriorityQueue<Vertex> pq, Edge e, Graph h) {
		Vertex u = e.getSource();
		Vertex v = e.getDestination();
		
		if (u.getD() + e.getWeight() < v.getD()) {
			pq.remove(v);
			v.setD(u.getD() + e.getWeight());
			
			for (Vertex w: v.getPi()) {
				h.removeEdge(v, w);
			}
			
			v.getPi().clear();
			v.addToPi(u);
			pq.add(v);
			
			h.addVertex(v);
			h.addEdge(u, v, 1);
		} else if (u.getD() + e.getWeight() == v.getD()) {
			pq.remove(v);
			v.setD(u.getD() + e.getWeight());
			v.addToPi(u);
			pq.add(v);
			
			h.addVertex(v);
			h.addEdge(u, v, 1);
		}
	}
}



class VertexComparator implements Comparator<Vertex> {

	@Override
	public int compare(Vertex u, Vertex v) {
		return (int)Math.ceil(u.getD()) - (int)Math.floor(v.getD());
	}
	
}
